package escript;


class EventEngine
{
    public var onEventDone: (evt: Event) -> Void;
    public var onEventPaused: (evt: Event) -> Void;
    public var onEventWaitCondition: (evt: Event) -> Void;
    public var onEventWaitInput: (evt: Event, prompt: Int) -> Void;

    var events: Array<Event>;
    var eventsToDispatch: Array<Event>;


    public function new()
    {
        events = new Array<Event>();
        eventsToDispatch = new Array<Event>();
    }


    /**
        Update currently running events by the given elapsed time.

        This method is also responsible for starting newly dispatched events.

        @param dt The time elapsed, in seconds.
    **/
    public function update(dt: Float)
    {
        // process dispatch list
        while (eventsToDispatch.length > 0)
        {
            var evt: Event = eventsToDispatch.pop();

            events.push(evt);
        }

        // update all events
        for (event in events)
        {
            var stateBeforeUpdate: EventState = event.state;

            event.update(dt);

            if (event.state == Paused && stateBeforeUpdate != Paused)
            {
                // event was just paused
                if (onEventPaused != null)
                {
                    onEventPaused(event);
                }
            }
            else if (event.state == WaitCondition && stateBeforeUpdate != WaitCondition)
            {
                // event was just put on wait for a condition
                if (onEventWaitCondition != null)
                {
                    onEventWaitCondition(event);
                }
            }
            else if (event.state == WaitInput && stateBeforeUpdate != WaitInput)
            {
                // event was just put on input wait
                if (onEventWaitInput != null)
                {
                    onEventWaitInput(event, event.inputPrompt);
                }
            }
        }

        // remove finished events
        var eventsDone: Array<Event> = events.filter(evt -> evt.state == Done);
        for (eventDone in eventsDone)
        {
            events.remove(eventDone);

            if (onEventDone != null)
            {
                onEventDone(eventDone);
            }

            if (eventDone.repeat)
            {
                eventDone.init();
                eventsToDispatch.push(eventDone);
            }
        }
    }


    /**
        Start a new event in the engine.

        New events are not started immediately by this method,
        they are instead initialized and started during the next call to `update()`.

        Repeating events run continuously. When done, the `onEventDone()` callback is invoked as normal,
        and the event is then re-initialized and dispatched again.

        @param evt The event to dispatch.
        @param repeat Whether this event is a repeating event.
    **/
    @:generic
    public inline function dispatch<T: Event>(evt: T, repeat: Bool = false): T
    {
        if (evt.state != None && evt.state != Done)
        {
            throw 'dispatching event with state: ${evt.state}';
        }

        evt.init();
        evt.repeat = repeat;

        eventsToDispatch.push(evt);
        return evt;
    }
}
