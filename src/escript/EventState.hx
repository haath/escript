package escript;


enum EventState
{
    /**
     * Event not yet dispatched on an event engine.
     */
    None;

    /**
     * Event has been dispatched on an event engine, but it has not yet started.
     */
    Dispatch;

    /**
     * Event is currently running.
     */
    Run;

    /**
     * Event has been suspended using the `pause()` function.
     * It will resume after `resume()` is called on the event.
     */
    Paused;

    /**
     * Event has been suspended using the `waitFor()` function.
     * It will resume after the specified condition is met.
     */
    WaitCondition;

    /**
     * Event has been suspended using the `input()` function.
     * It will resumt after `write()` is called on the event.
     */
    WaitInput;

    /**
     * The event has completed its execution.
     */
    Done;
}
