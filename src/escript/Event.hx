package escript;


@:allow(escript.EventEngine)
@:autoBuild(escript.EventBuilder.build())
class Event
{
    public var state(default, null): EventState;
    @:noCompletion var repeat: Bool;
    @:noCompletion var nextFunction: () -> Void;
    @:noCompletion var delayTimer: Float;
    @:noCompletion var paused: Bool;
    @:noCompletion var inputData: Dynamic;
    @:noCompletion var inputPrompt: Int;
    @:noCompletion var waitForCondition: Void->Bool;
    @:noCompletion var waitForSampleTime: Float;


    function new()
    {
        reset();
        state = None;
    }


    @:noCompletion
    inline final function reset()
    {
        state = Dispatch;
        nextFunction = null;
        delayTimer = 0;
        paused = false;
    }


    @:noCompletion
    final function init()
    {
        reset();

        nextFunction = start;
    }


    @:noCompletion
    final function update(dt: Float)
    {
        updateState();

        if (state == WaitCondition)
        {
            delayTimer -= dt;

            // check if sample time elapsed.
            if (delayTimer <= 0)
            {
                if (waitForCondition())
                {
                    // condition met, resume execution
                    state = Run;

                    waitForCondition = null;
                    waitForSampleTime = 0;
                }
                else
                {
                    // condition not met, reset delay to sample time
                    delayTimer = waitForSampleTime;
                }
            }
        }

        if (state != Run)
        {
            return;
        }

        delayTimer -= dt;

        if (delayTimer <= 0)
        {
            // invoke next script
            var func = nextFunction;
            nextFunction = null;

            func();
        }

        updateState();
    }


    @:noCompletion
    inline function updateState()
    {
        state = switch state
        {
            case Dispatch: Run;
            case Run if (paused): Paused;
            case Paused if (!paused): Run;
            case Run if (nextFunction == null): Done;
            case _: state;
        }
    }


    @IgnoreCover
    @:noCompletion
    function start()
    {
        // override for script start
    }


    // ======== EXTERNAL EVENT FUNCTIONS ========

    /**
        Resumes the event that has been paused with the `pause()` method.
    **/
    public final inline function resume()
    {
        paused = false;
    }


    /**
        Stops the event, setting its state to `Done`.
    **/
    public final inline function stop(cancelRepeat: Bool = false)
    {
        state = Done;

        if (cancelRepeat)
        {
            repeat = false;
        }
    }


    /**
        Write data to a script that is currently waiting for input.

        Will throw an error if the script is not currently waiting for input.

        @param inputData The input data to send to the script. Its value will be written to the
        variable on the left side of the `input()` call.
    **/
    public final inline function write(inputData: Dynamic)
    {
        if (state != WaitInput)
        {
            throw 'write() was called on an event in the $state state that is not waiting for input.';
        }

        this.inputData = inputData;
        state = Run;
    }


    // ======== INTERNAL EVENT FUNCTIONS ========

    /**
        Causes a delay in script execution for the given time in seconds.

        @param time The sleep time in seconds.
    **/
    final inline function sleep(time: Float)
    {
        delayTimer = time;
    }


    /**
        Pause script execution.
        The script will remain paused until `resume()` is called on the event.
    **/
    final inline function pause()
    {
        paused = true;
    }


    /**
        Pause script execution until a given condition is met.

        The `condition` callback is invoked on every update. When `true` is returned, script execution is resumed.

        Optionally, a sample time can be specified to limit how often the callback is checked.

        @param condition The condition to wait for. Execution will resume when this function returns `true`.
        @param sampleTime Time to allow between calls to the `condition` callback.
    **/
    final function waitFor(condition: Void->Bool, sampleTime: Float = 0)
    {
        state = WaitCondition;
        waitForCondition = condition;
        waitForSampleTime = sampleTime;
        delayTimer = sampleTime;
    }


    /**
        Pause script execution to wait for external input.

        Invokes the `onEventWaitInput` callback on the event engine.

        The event should then be resumed using `write()`.
        The value of the `inputData` argument given to `write()` will then be assigned to the variable
        on the left side of the assignment during the call to this method.

        The return type will be inferred from the assignment, therefore it is up to the user to
        pass the correct data type to `write()`.

        ```haxe
        var x: Int = input();
        var y: String = input();
        ```
    **/
    final function input<T>(prompt: Int = -1): T
    {
        if (state != Run)
        {
            throw 'input() was called on an event in the $state state.';
        }

        inputPrompt = prompt;
        state = WaitInput;
        return null;
    }


    /**
        Jumps to another script function.

        This method is the preferable way of creating branched script execution.

        @param script The event script method to jump to.
        @param delay Delay in seconds after which to jump to the next script.
    **/
    final inline function jump(script: () -> Void, delay: Float = 0)
    {
        nextFunction = script;
        delayTimer = delay;
    }
}
