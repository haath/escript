package escript;

#if (macro || doc_gen)
import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.ExprTools;


class EventBuilder
{
    public static macro function build(): Array<Field>
    {
        #if macro
        var fields: Array<Field> = Context.getBuildFields();

        // Look for script functions.
        var scriptFunctions: Array<Field> = fields.filter(field -> isScriptFunction(field));

        // Preprocess script functions.
        for (field in scriptFunctions)
        {
            validateScriptFunction(field);

            var func: Function = toFunction(field);
            func.expr.expr = recursiveReorderStatements(getFunctionBody(func));
        }

        return fields;
        #else
        return [];
        #end
    }


    #if macro
    /**
        Checks a field for two things:

        - Is a function.
        - Is not the `new()` or `init()` function.
    **/
    static function isScriptFunction(field: Field): Bool
    {
        switch field.kind
        {
            case FFun(func) if (field.name != "new" && field.name != "init"):
            {
                return true;
            }

            case _:
        }

        return false;
    }


    static function validateScriptFunction(field: Field)
    {
        validateBlock(toFunction(field).expr, true);
    }


    static function validateBlock(block: Expr, isTopLevel: Bool = false)
    {
        if (block == null)
        {
            return;
        }

        var block: Array<Expr> = switch block.expr
        {
            case EBlock(expr): expr;
            case _: return;
        }

        var check = (expr: Expr) ->
        {
            var name: String = getFunctionName(expr);

            if (!isTopLevel && isDelayFunction(expr))
            {
                Context.error('Delay function $name() can only be used at the top-level of a function, and not in nested blocks.', Context.currentPos());
            }
        }

        for (expr in block)
        {
            check(expr);

            switch expr.expr
            {
                case EWhile(econd, e, normalWhile): validateBlock(e);

                case EIf(econd, eif, eelse): validateBlock(eif); validateBlock(eelse);

                case EFunction(kind, f): validateBlock(f.expr);

                case EFor(it, expr): validateBlock(expr);

                case EBinop(op, e1, e2) if (op == OpAssign):
                {
                    check(expr);
                    validateBlock(e2);
                }

                case _:
            }
        }
    }


    static function recursiveReorderStatements(body: Array<Expr>): ExprDef
    {
        var newBody: Array<Expr> = [];

        for (i in 0...body.length)
        {
            var expr: Expr = body[i];

            newBody.push(expr);

            if (isDelayFunction(expr))
            {
                /**
                    Encountered delay call, put all following expressions into a callback.

                    A delay call at the end of the script will not generate any callback,
                    however the method will still be called to set the time delay, in order to
                    delay when onEventDone is called for this event.
                **/
                var callback: Expr = addExpressionsToCallback(body.slice(i + 1), expr);

                newBody.push(callback);

                break;
            }
            else if (isAbortFunction(expr))
            {
                // Add return expression after an abort function.
                var returnExpr: Expr = Context.parse('return;', Context.currentPos());

                newBody.push(returnExpr);

                break;
            }
        }

        return EBlock(newBody);
    }


    /**
        Returns `true` if the given expression is a call of the `sleep()`, `pause()` or `input()` functions,
        which delay execution of the current script.
    **/
    static inline function isDelayFunction(expr: Expr): Bool
    {
        return ["sleep", "pause", "input", "waitFor"].filter(n -> isCallOfFunction(expr, n)).length > 0;
    }


    /**
        Returns `true` if the given expression is a call of the `jump()` or the `stop()` function,
        which abort execution of the current script.
    **/
    static inline function isAbortFunction(expr: Expr): Bool
    {
        return ["jump", "stop"].filter(n -> isCallOfFunction(expr, n)).length > 0;
    }


    static function addExpressionsToCallback(exprs: Array<Expr>, lastExpr: Expr): Expr
    {
        if (exprs.length == 0)
        {
            return macro {};
        }

        var callback: Function = {
            ret: macro: Void,
            expr: toExpr(recursiveReorderStatements(exprs)),
            args: []
        };

        if (isCallOfFunction(lastExpr, "input"))
        {
            /**
                This delay call is the input() function.

                The first expression in the callback must be the assignment of inputData into the target variable.
            **/
            var callBackBody: Array<Expr> = getFunctionBody(callback);
            callBackBody.insert(0, toExpr(EBinop(
                OpAssign,
                getAssignmentLeftOperand(lastExpr),
                macro cast inputData
            )));

            callback.expr.expr = EBlock(callBackBody);
        }

        return {
            pos: Context.currentPos(),
            expr: EBinop(
                OpAssign,
                macro nextFunction,
                toExpr(EFunction(
                    FAnonymous,
                    callback
                ))
            )
        };
    }


    static function getAssignmentLeftOperand(expr: Expr): Expr
    {
        switch expr.expr
        {
            case EBinop(op, e1, e2): return e1;
            case EVars(vars): return macro $i{vars[0].name};
            case _: throw "Not assignment.";
        }
        return null;
    }


    static function toExpr(exprDef: ExprDef): Expr
    {
        return {
            pos: Context.currentPos(),
            expr: exprDef
        };
    }


    static function isCallOfFunction(expr: Expr, functionName: String): Bool
    {
        return getFunctionName(expr) == functionName;
    }


    static function getFunctionName(expr: Expr): String
    {
        switch expr.expr
        {
            case ECall(e, params):
            {
                switch e.expr
                {
                    case EConst(c):
                    {
                        switch c
                        {
                            case CIdent(s):
                            {
                                return s;
                            }
                            case _:
                        }
                    }
                    case _:
                }
            }

            case EVars(vars):
            {
                for (v in vars)
                {
                    if (v.expr != null)
                    {
                        var n: String = getFunctionName(v.expr);
                        if (n != null)
                        {
                            return n;
                        }
                    }
                }
            }

            case EBinop(op, e1, e2):
            {
                return getFunctionName(e2);
            }

            case _:
        }

        return null;
    }


    static function toFunction(field: Field): Function
    {
        return switch field.kind
        {
            case FFun(func): func;
            case _: Context.error("Not a function.", Context.currentPos());
        }
    }


    static function getFunction(fields: Array<Field>, name: String): Function
    {
        for (field in fields)
        {
            switch field.kind
            {
                case FFun(func) if (field.name == name): return func;
                case _:
            }
        }
        return null;
    }


    static function getFunctionBody(func: Function): Array<Expr>
    {
        return switch func.expr.expr
        {
            case EBlock(exprs): exprs;
            case _: null;
        }
    }


    inline static function print(s: Dynamic)
    {
        Sys.println(s);
    }
    #end
}
#end

