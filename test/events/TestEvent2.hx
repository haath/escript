package events;

import escript.Event;


class TestEvent2 extends Event
{
    public var eventsCalled: Array<Int>;

    public var cancelRepeat: Bool;


    public function new()
    {
        super();
        eventsCalled = new Array<Int>();
        cancelRepeat = false;
    }


    override function start()
    {
        eventsCalled = new Array<Int>();

        eventsCalled.push(0);

        sleep(1);

        eventsCalled.push(1);

        sleep(2);

        eventsCalled.push(2);

        stop(cancelRepeat);
    }
}
