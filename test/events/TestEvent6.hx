package events;

import escript.Event;


class TestEvent6 extends Event
{
    var eventsCalled: Array<Int>;
    var sensorValue: Int;


    public function new()
    {
        super();
        eventsCalled = new Array<Int>();
        sensorValue = 0;
    }


    override function start()
    {
        eventsCalled.push(0);

        waitFor(() -> sensorValue > 5);

        eventsCalled.push(1);

        waitFor(() -> sensorValue > 10, 2);

        eventsCalled.push(2);
    }
}
