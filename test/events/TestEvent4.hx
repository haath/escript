package events;

import escript.Event;


class TestEvent4 extends Event
{
    var eventsCalled: Array<Int>;

    var target: Int;

    public function new(target: Int)
    {
        super();
        eventsCalled = new Array<Int>();
        this.target = target;
    }


    override function start()
    {
        if (target == 0)
        {
            jump(event0, 0);
        }
        else if (target == 1)
        {
            jump(event1, 1);
        }
        else if (target == 2)
        {
            jump(event2, 2);
        }
    }


    function event0()
    {
        eventsCalled.push(0);
    }


    function event1()
    {
        eventsCalled.push(1);
    }


    function event2()
    {
        eventsCalled.push(2);
    }
}
