package events;

import escript.Event;


class TestEvent3 extends Event
{
    public var eventsCalled: Array<Int>;


    public function new()
    {
        super();
        eventsCalled = new Array<Int>();
    }


    override function start()
    {
        eventsCalled = new Array<Int>();

        eventsCalled.push(0);

        pause();

        eventsCalled.push(1);
    }
}
