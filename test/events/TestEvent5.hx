package events;

import escript.Event;


class TestEvent5 extends Event
{
    var eventsCalled: Array<Int>;


    public function new()
    {
        super();
        eventsCalled = new Array<Int>();
    }


    override function start()
    {
        eventsCalled.push(0);

        var x: Int = input();

        eventsCalled.push(x);

        var y: Int = input();

        eventsCalled.push(y);
    }
}
