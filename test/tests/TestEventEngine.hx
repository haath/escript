package tests;

import events.*;
import utest.Assert;
import escript.EventEngine;
import utest.ITest;
import escript.EventState.*;


@:access(escript.EventEngine)
class TestEventEngine implements ITest
{
    public function new() { }


    function testEventEngine()
    {
        var eng: EventEngine = new EventEngine();

        var evt: TestEvent2 = eng.dispatch(new TestEvent2());

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);

        Assert.raises(() -> eng.dispatch(evt), String);

        eng.update(0);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
        Assert.equals(1, evt.eventsCalled.length);

        eng.dispatch(new TestEvent1());

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);

        eng.update(1);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
        Assert.equals(2, evt.eventsCalled.length);

        eng.update(2);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);
        Assert.equals(3, evt.eventsCalled.length);
    }


    function testEventEngineRepeating()
    {
        var eng: EventEngine = new EventEngine();

        var evt: TestEvent2 = eng.dispatch(new TestEvent2(), true);

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);
        Assert.equals(Dispatch, evt.state);
        arrayEquals([], evt.eventsCalled);

        eng.update(0);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
        Assert.equals(Run, evt.state);
        arrayEquals([0], evt.eventsCalled);

        eng.update(1);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
        Assert.equals(Run, evt.state);
        arrayEquals([0, 1], evt.eventsCalled);

        eng.update(2);

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);
        Assert.equals(Dispatch, evt.state);
        arrayEquals([0, 1, 2], evt.eventsCalled);

        eng.update(0);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
        Assert.equals(Run, evt.state);
        arrayEquals([0], evt.eventsCalled);

        evt.cancelRepeat = true;
        eng.update(1);
        eng.update(2);
        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);
        Assert.equals(Done, evt.state);
        arrayEquals([0, 1, 2], evt.eventsCalled);
    }


    function testEventEnginePause()
    {
        var eng: EventEngine = new EventEngine();

        var evt3: TestEvent3 = eng.dispatch(new TestEvent3());

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);

        eng.update(10);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);

        eng.update(10);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);

        eng.update(10);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
    }


    function testEventEngineInput()
    {
        var eng: EventEngine = new EventEngine();

        var evt5: TestEvent5 = eng.dispatch(new TestEvent5());

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);

        eng.update(10);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);

        eng.update(10);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
    }


    function testEventWaitCondition()
    {
        var eng: EventEngine = new EventEngine();

        var evt6: TestEvent6 = eng.dispatch(new TestEvent6());

        Assert.equals(1, eng.eventsToDispatch.length);
        Assert.equals(0, eng.events.length);

        eng.update(10);

        Assert.equals(0, eng.eventsToDispatch.length);
        Assert.equals(1, eng.events.length);
    }


    function testEventEngineCallbacks()
    {
        var eng: EventEngine = new EventEngine();

        var evt1: TestEvent1 = eng.dispatch(new TestEvent1());
        var evt2: TestEvent2 = eng.dispatch(new TestEvent2());
        var evt3: TestEvent3 = eng.dispatch(new TestEvent3());
        var evt5: TestEvent5 = eng.dispatch(new TestEvent5());
        var evt6: TestEvent6 = eng.dispatch(new TestEvent6());

        var evt1Done: Bool = false;
        var evt2Done: Bool = false;
        var evt3Paused: Bool = false;
        var evt5WaitInput: Bool = false;
        var evt6WaitCondition: Bool = false;

        eng.onEventDone = (evt) ->
        {
            if (evt == evt1)
            {
                Assert.isFalse(evt1Done);
                evt1Done = true;
            }
            else if (evt == evt2)
            {
                Assert.isFalse(evt2Done);
                evt2Done = true;
            }
            else if (evt == evt3)
            {
                Assert.fail();
            }
        };
        eng.onEventPaused = (evt) ->
        {
            if (evt == evt1)
            {
                Assert.fail();
            }
            else if (evt == evt2)
            {
                Assert.fail();
            }
            else if (evt == evt3)
            {
                Assert.isFalse(evt3Paused);
                evt3Paused = true;
            }
        };
        eng.onEventWaitCondition = (evt) ->
        {
            if (evt == evt6)
            {
                Assert.isFalse(evt6WaitCondition);
                evt6WaitCondition = true;
            }
            else
            {
                Assert.fail();
            }
        }
        eng.onEventWaitInput = (evt, p) ->
        {
            if (evt == evt5)
            {
                Assert.isFalse(evt5WaitInput);
                evt5WaitInput = true;
            }
            else
            {
                Assert.fail();
            }
        }

        eng.update(0);
        Assert.isTrue(evt1Done);
        Assert.isTrue(evt5WaitInput);
        Assert.isTrue(evt6WaitCondition);

        eng.update(0);
        Assert.isTrue(evt3Paused);

        eng.update(2);
        eng.update(2);
        Assert.isTrue(evt2Done);
    }


    function arrayEquals<T>(expected: Array<T>, value: Array<T>)
    {
        Assert.equals(expected.length, value.length);

        for (i in 0...expected.length)
        {
            Assert.equals(expected[i], value[i], '$i');
        }
    }
}
