package tests;

import haxe.CallStack;
import escript.EventState;
import events.*;
import utest.Assert;
import utest.ITest;


@:access(escript.Event)
class TestEvent implements ITest
{
    public function new() { }


    function testSleep()
    {
        var evt: TestEvent2 = new TestEvent2();

        arrayEquals([], evt.eventsCalled);
        Assert.equals(EventState.None, evt.state);

        evt.init();
        evt.update(0);

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Run, evt.state);

        evt.update(0.5);

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Run, evt.state);

        evt.update(0.5);

        arrayEquals([0, 1], evt.eventsCalled);
        Assert.equals(EventState.Run, evt.state);

        evt.update(0.5);

        arrayEquals([0, 1], evt.eventsCalled);
        Assert.equals(EventState.Run, evt.state);

        evt.update(2);

        arrayEquals([0, 1, 2], evt.eventsCalled);
        Assert.equals(EventState.Done, evt.state);

        evt.update(10);

        arrayEquals([0, 1, 2], evt.eventsCalled);
        Assert.equals(EventState.Done, evt.state);
    }


    function testPause()
    {
        var evt: TestEvent3 = new TestEvent3();

        arrayEquals([], evt.eventsCalled);
        Assert.equals(EventState.None, evt.state);

        evt.init();
        evt.update(0);

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Paused, evt.state);

        evt.update(0);

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Paused, evt.state);

        evt.resume();

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Paused, evt.state);

        evt.update(0);

        arrayEquals([0, 1], evt.eventsCalled);
        Assert.equals(EventState.Done, evt.state);
    }


    function testStop()
    {
        var evt: TestEvent3 = new TestEvent3();

        arrayEquals([], evt.eventsCalled);
        Assert.equals(EventState.None, evt.state);

        evt.init();
        evt.update(0);

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Paused, evt.state);

        evt.stop();

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Done, evt.state);

        evt.update(10);

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Done, evt.state);

        evt.resume();

        arrayEquals([0], evt.eventsCalled);
        Assert.equals(EventState.Done, evt.state);
    }


    function testJump()
    {
        var evt0: TestEvent4 = new TestEvent4(0);
        var evt1: TestEvent4 = new TestEvent4(1);
        var evt2: TestEvent4 = new TestEvent4(2);

        evt0.init();
        evt0.update(0);
        Assert.equals(EventState.Run, evt0.state);
        evt1.init();
        evt1.update(0);
        Assert.equals(EventState.Run, evt1.state);
        evt2.init();
        evt2.update(0);
        Assert.equals(EventState.Run, evt2.state);

        evt0.update(0);
        Assert.equals(EventState.Done, evt0.state);
        arrayEquals([0], evt0.eventsCalled);
        evt1.update(0.5);
        Assert.equals(EventState.Run, evt1.state);
        arrayEquals([], evt1.eventsCalled);
        evt2.update(2);
        Assert.equals(EventState.Done, evt2.state);
        arrayEquals([2], evt2.eventsCalled);
    }


    function testInput()
    {
        var evt: TestEvent5 = new TestEvent5();

        evt.init();
        Assert.equals(EventState.Dispatch, evt.state);
        arrayEquals([], evt.eventsCalled);

        Assert.raises(() -> evt.write(2), String);

        evt.update(0);
        Assert.equals(EventState.WaitInput, evt.state);
        arrayEquals([0], evt.eventsCalled);

        Assert.raises(() -> evt.input(), String);

        evt.write(5);
        Assert.equals(EventState.Run, evt.state);
        arrayEquals([0], evt.eventsCalled);

        evt.update(0);
        Assert.equals(EventState.WaitInput, evt.state);
        arrayEquals([0, 5], evt.eventsCalled);

        evt.write(7);
        Assert.equals(EventState.Run, evt.state);
        arrayEquals([0, 5], evt.eventsCalled);

        evt.update(0);
        Assert.equals(EventState.Done, evt.state);
        arrayEquals([0, 5, 7], evt.eventsCalled);
    }


    function testWaitFor()
    {
        var evt: TestEvent6 = new TestEvent6();

        evt.init();
        Assert.equals(EventState.Dispatch, evt.state);
        arrayEquals([], evt.eventsCalled);

        evt.update(0);
        Assert.equals(EventState.WaitCondition, evt.state);
        arrayEquals([0], evt.eventsCalled);

        evt.sensorValue = 4;

        evt.update(2);
        Assert.equals(EventState.WaitCondition, evt.state);
        arrayEquals([0], evt.eventsCalled);

        evt.sensorValue = 6;

        evt.update(0);
        Assert.equals(EventState.WaitCondition, evt.state);
        arrayEquals([0, 1], evt.eventsCalled);

        evt.sensorValue = 12;

        evt.update(1);
        Assert.equals(EventState.WaitCondition, evt.state);
        arrayEquals([0, 1], evt.eventsCalled);

        evt.update(1);
        Assert.equals(EventState.Done, evt.state);
        arrayEquals([0, 1, 2], evt.eventsCalled);
    }


    function arrayEquals<T>(expected: Array<T>, value: Array<T>)
    {
        Assert.equals(expected.length, value.length);

        for (i in 0...expected.length)
        {
            Assert.equals(expected[i], value[i], '$i');
        }
    }
}
